/* Copyright (C) : 2014-2019
   European Synchrotron Radiation Facility
   BP 220, Grenoble 38043, FRANCE

   This file is part of libhdb++.

   libhdb++ is free software: you can redistribute it and/or modify
   it under the terms of the Lesser GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   libhdb++ is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
   GNU General Public License for more details.

   You should have received a copy of the Lesser GNU General Public License
   along with libhdb++.  If not, see <http://www.gnu.org/licenses/>. */


#include "HdbppSqliteDbApi.hpp"
#include <sqlite3.h>
#include "Stmt.hpp"
#include <numeric>

namespace hdbpp
{
namespace details
{
    template<class T>
    struct sqlite_type_traits
    {
        using sqlite_type = T;
    };

    template<>
    struct sqlite_type_traits<bool>
    {
        using sqlite_type = int;
    };

    template<>
    struct sqlite_type_traits<short>
    {
        using sqlite_type = int;
    };

    template<>
    struct sqlite_type_traits<long>
    {
        using sqlite_type = double;
    };
    
    template<>
    struct sqlite_type_traits<unsigned char>
    {
        using sqlite_type = int;
    };
    
    template<>
    struct sqlite_type_traits<unsigned short>
    {
        using sqlite_type = int;
    };
    
    template<>
    struct sqlite_type_traits<unsigned int>
    {
        using sqlite_type = int;
    };
    
    template<>
    struct sqlite_type_traits<unsigned long>
    {
        using sqlite_type = double;
    };


    template<class T>
    typename hdbpp::details::sqlite_type_traits<T>::sqlite_type convert(std::vector<T>& values) 
    {
        return static_cast<typename hdbpp::details::sqlite_type_traits<T>::sqlite_type> (values[0]);
    }

    template<class T>
    std::string convertToString(std::vector<T>& values, int size)
    {
        if (size >= 1) {
            std::ostringstream result_vector;
            std::string result;
            for (size_t i=0; i < size - 1; i++) {
                result_vector << static_cast<typename hdbpp::details::sqlite_type_traits<T>::sqlite_type> (values[i]);
                result_vector << ", ";
            }
            result_vector << values[size - 1] << std::ends;
            result = result_vector.str();
            return result;
        }
        return "No values";
    }
}

// Template declaration for the extraction of the value_r and value_w
template<class T>
void extract_value_r(Stmt &stmt, Tango::DeviceAttribute& attr, int write_type, int att_conf_format, int size) 
{
    if (write_type == 1 || write_type == 2 || write_type == 4) {
        std::vector<T> extract;
        attr.extract_read(extract);
        if (att_conf_format == 1) {
            stmt.linkPara(5, hdbpp::details::convert(extract));
        } else if (att_conf_format == 2) {
            stmt.array_result_r = hdbpp::details::convertToString(extract, size);
            stmt.linkPara(5, stmt.array_result_r);
        }
    }
}
template<class T>
void extract_value_w(Stmt &stmt, Tango::DeviceAttribute& attr, int write_type, int att_conf_format, int index_w, int size)
{
    if (write_type == 2 || write_type == 3 || write_type == 4)
    {
	    std::vector<T> extract;
	    attr.extract_set(extract);
        if (att_conf_format == 1) {
	        stmt.linkPara(index_w, hdbpp::details::convert(extract));
        } else if (att_conf_format == 2) {
            stmt.array_result_w = hdbpp::details::convertToString(extract, size);
            stmt.linkPara(index_w, stmt.array_result_w);
        }
    }
}

//=============================================================================
//=============================================================================
HdbppSqliteDbApi::HdbppSqliteDbApi(const std::string &id, const std::vector<std::string> &configuration)
: db(nullptr)
{
    int rc;
    const std::string& dbName = configuration[0];
    rc = sqlite3_open_v2(dbName.c_str(), &db, SQLITE_OPEN_READWRITE, NULL);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error opening the database : " + std::string(sqlite3_errmsg(db)));
    }
}

//=============================================================================
//=============================================================================
HdbppSqliteDbApi::~HdbppSqliteDbApi()
{
    if (db != nullptr) {
        sqlite3_close(db);
    }
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insert_event(Tango::EventData *event_data, const HdbEventDataType &data_type)
{
    int att_conf_id;
    std::string table_name;
    int att_error_desc_id = 0;
    int write_type;
    int att_conf_format;
    int quality = event_data->attr_value->quality;
    int data_t = event_data->attr_value->data_type;

    if (!attributeExists(event_data->attr_name, att_conf_id, table_name, data_t, write_type, event_data->attr_value->data_format,att_conf_format)) {
        throw std::runtime_error("The attribute does not exist in the database");
    }
    if (event_data->err == true) {
        std::string error_msg(event_data->errors[0].desc);
        if (!errorMsgExists(error_msg, att_error_desc_id)) {
            archiveErrorMessage(error_msg, att_error_desc_id);
        }
    }
    archiveAttributeData(event_data, att_conf_id, att_error_desc_id, table_name, data_t, write_type, quality, att_conf_format);
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insert_events(std::vector<std::tuple<Tango::EventData *, HdbEventDataType>> events) 
{
    bool eventsInsertion = true;

    // Beginning of the transaction (group insertion)
    sqlite3_exec(db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);
    try {
        Tango::EventData *event_data;
        HdbEventDataType data_type;
        for (const auto &event : events) {
            event_data = std::get<0>(event);
            data_type = std::get<1>(event);
            insert_event(event_data, data_type);
        }
        if (eventsInsertion) {
            // Commit the group insertion
            sqlite3_exec(db, "COMMIT;", nullptr, nullptr, nullptr);
        }
    } catch (const std::runtime_error& e) {
        // Annulation of the group insertion if failed and indivual insertion will start
        sqlite3_exec(db, "ROLLBACK;", nullptr, nullptr, nullptr);
        eventsInsertion = false;
    }

    // Start of the individual insertion if the group insertion didn't work
    if (!eventsInsertion) {
        for (const auto &event : events) {
            Tango::EventData *event_data = std::get<0>(event);
            HdbEventDataType data_type = std::get<1>(event);
            try {
                insert_event(event_data, data_type);
                std::cout << "Insertion of the event successfull : " << event_data->attr_name << std::endl;
            }
            catch(const std::exception &e) {
                std::cout << "Insertion of the event failed : " << e.what() << std::endl;
            }
        }
    }
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insert_param_event(Tango::AttrConfEventData *param_event, const HdbEventDataType &data_type)
{
    int att_conf_id;
    std::string table_name;
    int att_error_desc_id = 0;
    int write_type;
    int att_conf_format;
    int data_t = param_event->attr_conf->data_type;

    if (!attributeExists(param_event->attr_name, att_conf_id, table_name, data_t, write_type, param_event->attr_conf->data_format, att_conf_format)) {
        throw std::runtime_error("The attribute does not exist in the database");
    }
    if (param_event->err == true) {
        std::string error_msg(param_event->errors[0].desc);
        if (!errorMsgExists(error_msg, att_error_desc_id)) {
            archiveErrorMessage(error_msg, att_error_desc_id);
        }
    }
    insertAttParam(param_event, att_conf_id, data_t, att_conf_format);
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::add_attribute(const std::string &fqdn_attr_name, int type, int format, int write_type)
{
    int att_conf_id;
    std::string table_name;
    int att_error_desc_id = 0;
    int att_conf_format;

    if (attributeExists(fqdn_attr_name, att_conf_id, table_name, type, write_type, format, att_conf_format)) {
        throw std::runtime_error("The attribute already exists in the database");
    }
    getTableName(type, table_name, att_conf_format);
    insertAttConf(fqdn_attr_name, type, format, write_type, table_name);
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::update_ttl(const std::string &fqdn_attr_name, unsigned int ttl)
{
    // Create the statement
    std::string query = "UPDATE att_conf SET ttl = ? WHERE att_name = '" + fqdn_attr_name + "';";
    Stmt stmt(db, query); 

    // Unsigned int to int to use the linkPara (int) method
    int ttl_int = static_cast<int>(ttl);

    // Link the parameter with the name of the attribute
    stmt.linkPara(1, ttl_int);

    // Execute SQL statement
    stmt.execute();
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insert_history_event(const std::string &fqdn_attr_name, unsigned char event)
{
    if (!eventExists(fqdn_attr_name)) {
        throw std::runtime_error("The attribute does not exist in the database");
    }
    try {
        insertAttHistoryEvent(event);
    } catch (const std::exception& e) {
        std::cout << "Insertion of the event history failed : " << e.what() << std::endl;
    }
}

//=============================================================================
//=============================================================================
bool HdbppSqliteDbApi::supported(HdbppFeatures feature)
{
    switch (feature) {
        case HdbppFeatures::TTL:
            return true;
        case HdbppFeatures::BATCH_INSERTS:
            return true;
        default:
            return false;
    }
}

//=============================================================================
//=============================================================================
bool HdbppSqliteDbApi::attributeExists(const std::string &attr_name, int &att_conf_id, std::string &table_name, int data_type, int &write_type, int format, int &att_conf_format)
{
    // Retrieve the right att_conf_type_id to compare
    std::string query_type = "SELECT att_conf_type_id FROM att_conf_type WHERE type_num = ?;";
    Stmt stmt_type(db, query_type);
    stmt_type.linkPara(1, data_type);
    stmt_type.execute();
    int att_type_id = stmt_type.getColumnInt(0);

    // Retrieve the right format
    std::string query_format = "SELECT att_conf_format_id FROM att_conf_format WHERE format_num = ?;";
    Stmt stmt_format(db, query_format);
    stmt_format.linkPara(1, format);
    stmt_format.execute();
    att_conf_format = stmt_format.getColumnInt(0);

    // Create the statement
    std::string query = "SELECT att_conf_id, att_conf_type_id, att_conf_format_id, att_conf_write_id, table_name FROM att_conf WHERE att_name = ?;";
    Stmt stmt(db, query);

    // Link the parameter with the name of the attribute
    stmt.linkPara(1, attr_name);

    // Execute SQL statement
    if (stmt.execute()) {
        int att_conf_type_id = stmt.getColumnInt(1);
        if (att_conf_type_id != att_type_id) {
            throw std::runtime_error("The data_type does not match the one in the table.");
        }
        att_conf_id = stmt.getColumnInt(0);
        att_conf_format = stmt.getColumnInt(2);
        write_type = stmt.getColumnInt(3);
        table_name = stmt.getColumnString(4);
        
        return true;
    }
    return false;
}

//=============================================================================
//=============================================================================
bool HdbppSqliteDbApi::errorMsgExists(std::string error_msg, int &att_error_desc_id)
{
    // Create the statement
    std::string query = "SELECT att_error_desc_id FROM att_error_desc WHERE error_desc = ?;";
    Stmt stmt(db, query);

    // Link the parameter with the name of the attribute
    stmt.linkPara(1, error_msg);

    // Execute SQL statement
    if (stmt.execute()) {
        att_error_desc_id = stmt.getColumnInt(0);
        return true;
    }
    return false;
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::archiveErrorMessage(std::string error_msg, int &att_error_desc_id)
{
    // Create the statement
    std::string query = "INSERT INTO att_error_desc (error_desc) VALUES (?);";
    Stmt stmt(db, query); 

    // Link the parameter with the name of the attribute
    stmt.linkPara(1, error_msg);

    // Execute SQL statement
    stmt.execute();

    // Get the att_error_desc_id of the new archived error message
    std::string query_att_error = "SELECT att_error_desc_id FROM att_error_desc WHERE error_desc = ?;";
    Stmt stmt_att_error(db, query_att_error); 
    stmt_att_error.linkPara(1, error_msg);
    stmt_att_error.execute();
    att_error_desc_id = stmt_att_error.getColumnInt(0);
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::archiveAttributeData(Tango::EventData *event_data, int att_conf_id, int att_error_desc_id, std::string table_name, int data_type, int write_type, int quality, int att_conf_format)
{

    // Index for the query
    int index_w;

    // Get the dimension of the vector
    Tango::AttributeDimension size_r = event_data->attr_value->get_r_dimension();
    long size_r_x = size_r.dim_x;

    Tango::AttributeDimension size_w = event_data->attr_value->get_w_dimension();
    long size_w_x = size_w.dim_x;

    // Create the statement
    std::string query;
    if (write_type == 1) {
        query = "INSERT INTO " + table_name + " (att_conf_id, data_time, att_error_desc_id, quality, value_r) VALUES (?, (STRFTIME('%Y-%m-%d %H:%M:%f',?/1000000000.0, 'unixepoch')), ?, ?, ?);";
    } else if (write_type == 3) {
        query = "INSERT INTO " + table_name + " (att_conf_id, data_time, att_error_desc_id, quality, value_w) VALUES (?, (STRFTIME('%Y-%m-%d %H:%M:%f',?/1000000000.0, 'unixepoch')), ?, ?, ?);";
        index_w = 5;
    } else {
        query = "INSERT INTO " + table_name + " (att_conf_id, data_time, att_error_desc_id, quality, value_r, value_w) VALUES (?, (STRFTIME('%Y-%m-%d %H:%M:%f',?/1000000000.0, 'unixepoch')), ?, ?, ?, ?);";
        index_w = 6;
    }
    Stmt stmt(db, query);
    
    // Convert TimeVal to string for the insertion in the database (YYYY-MM-DD HH:MM:SS.SSS)
    Tango::TimeVal &data_time = event_data->get_date();
    int64_t tot = static_cast<int64_t>(data_time.tv_sec)*1000000000 + static_cast<int64_t>(data_time.tv_usec)*1000 + static_cast<int64_t>(data_time.tv_nsec);

    // Link the parameters
    stmt.linkPara(1, att_conf_id);
    stmt.linkPara(2, tot);
    stmt.linkPara(3, att_error_desc_id);
    stmt.linkPara(4, quality);
    
    Tango::DeviceAttribute attr = *event_data->attr_value;
    // Switch to the right type for the valur_r and value_w extraction and link
    switch(data_type) {
        case 1:
            extract_value_r<bool>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<bool>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 2:
            extract_value_r<short>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<short>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 3:
            extract_value_r<int>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<int>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 23:
            extract_value_r<long>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<long>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 4:
            extract_value_r<float>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<float>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 5:
            extract_value_r<double>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<double>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 22:
            extract_value_r<unsigned char>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<unsigned char>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 6:
            extract_value_r<unsigned short>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<unsigned short>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 7:
            extract_value_r<unsigned int>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<unsigned int>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 24:
            extract_value_r<unsigned long>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<unsigned long>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
        case 8:
            extract_value_r<std::string>(stmt, attr, write_type, att_conf_format, size_r_x);
            extract_value_w<std::string>(stmt, attr, write_type, att_conf_format, index_w, size_w_x);
            break;
    }

    // Execute SQL statementatt_conf_id,
    stmt.execute();
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::getTableName(int type, std::string &table_name, int att_conf_format)
{
    std::map < int, std::string > dico_type;
    dico_type[1] = "devboolean";
    dico_type[2] = "devshort";
    dico_type[3] = "devlong";
    dico_type[4] = "devfloat";
    dico_type[5] = "devdouble";
    dico_type[6] = "devushort";
    dico_type[7] = "devulong";
    dico_type[8] = "devstring";
    dico_type[19] = "devstate";
    dico_type[22] = "devuchar";
    dico_type[23]= "devlong64";
    dico_type[24] = "devulong64";
    dico_type[28] = "devencoded";
    dico_type[30] = "devenum";

    if (att_conf_format == 1) {
        table_name = "att_scalar_" + dico_type[type];
    } else if (att_conf_format == 2) {
        table_name = "att_array_" + dico_type[type];
    }
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insertAttConf(const std::string &fqdn_attr_name, int type, int format, int write_type, std::string table_name)
{
    // Create the statement
    std::string query = "INSERT INTO att_conf (att_name, att_conf_type_id, att_conf_format_id, att_conf_write_id, table_name, cs_name, domain, family, member, name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    Stmt stmt(db, query);

    // Separate the name
    std::vector<std::string> vec_names;
    std::string sub_chain = fqdn_attr_name.substr(8);
    std::istringstream flux(sub_chain);
    std::string parts;

    while (std::getline(flux, parts, '/')) {
        vec_names.push_back(parts);
    }

    std::string cs_name = vec_names[0];
    std::string domain = vec_names[1];
    std::string family = vec_names[2];
    std::string member = vec_names[3];
    std::string name = vec_names[4];

    // Retrieve att_conf_type_id
    std::string query_t = "SELECT att_conf_type_id FROM att_conf_type WHERE type_num = ?;";
    Stmt stmt_t(db, query_t);
    stmt_t.linkPara(1, type);
    stmt_t.execute();
    int att_conf_type_id = stmt_t.getColumnInt(0);

    // Retrieve att_conf_format_id
    std::string query_f = "SELECT att_conf_format_id FROM att_conf_format WHERE format_num = ?;";
    Stmt stmt_f(db, query_f);
    stmt_f.linkPara(1, format);
    stmt_f.execute();
    int att_conf_format_id = stmt_f.getColumnInt(0);

    // Retrieve att_conf_write_id
    std::string query_w = "SELECT att_conf_write_id FROM att_conf_write WHERE write_num = ?;";
    Stmt stmt_w(db, query_w);
    stmt_w.linkPara(1, write_type);
    stmt_w.execute();
    int att_conf_write_id = stmt_w.getColumnInt(0);

    // Link the parameters
    stmt.linkPara(1, fqdn_attr_name);
    stmt.linkPara(2, att_conf_type_id);
    stmt.linkPara(3, att_conf_format_id);
    stmt.linkPara(4, att_conf_write_id);
    stmt.linkPara(5, table_name);
    stmt.linkPara(6, cs_name);
    stmt.linkPara(7, domain);
    stmt.linkPara(8, family);
    stmt.linkPara(9, member);
    stmt.linkPara(10, name);

    // Execute SQL statement
    stmt.execute();
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insertAttParam(Tango::AttrConfEventData *param_event, int att_conf_id, int data_type, int att_conf_format) 
{
    // Create the statement
    std::string query = "INSERT INTO att_parameter (att_conf_id, recv_time, label, unit, standard_unit, display_unit, format, archive_rel_change, archive_abs_change, archive_period, description, enum_labels) VALUES (?, (STRFTIME('%Y-%m-%d %H:%M:%f',?/1000000000.0, 'unixepoch')), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    Stmt stmt(db, query);
   
    // Convert TimeVal to string for the insertion in the database (YYYY-MM-DD HH:MM:SS.SSS)
    Tango::TimeVal &data_time = param_event->get_date();
    int64_t tot = static_cast<int64_t>(data_time.tv_sec)*1000000000 + static_cast<int64_t>(data_time.tv_usec)*1000 + static_cast<int64_t>(data_time.tv_nsec);

    // Convert the vector of string into a string
    std::vector<std::string> vec_enum_labels = param_event->attr_conf->enum_labels;
    std::string str_enum_labels = std::accumulate(vec_enum_labels.begin(), vec_enum_labels.end(), std::string(), 
                                [](std::string lhs, const std::string &rhs) { return lhs.empty() ? rhs : lhs + ", " + rhs; });
    
    // Link the parameters
    stmt.linkPara(1, att_conf_id);
    stmt.linkPara(2, tot);
    stmt.linkPara(3, param_event->attr_conf->label);
    stmt.linkPara(4, param_event->attr_conf->unit);
    stmt.linkPara(5, param_event->attr_conf->standard_unit);
    stmt.linkPara(6, param_event->attr_conf->display_unit);
    stmt.linkPara(7, att_conf_format);
    stmt.linkPara(8, param_event->attr_conf->events.arch_event.archive_rel_change);
    stmt.linkPara(9, param_event->attr_conf->events.arch_event.archive_abs_change);
    stmt.linkPara(10, param_event->attr_conf->events.arch_event.archive_period);
    stmt.linkPara(11, param_event->attr_conf->description);
    stmt.linkPara(12, str_enum_labels);

    // Execute SQL statement
    stmt.execute();
}


//=============================================================================
//=============================================================================
bool HdbppSqliteDbApi::eventExists(const std::string &attr_name)
{
    // Create the statement
    std::string query = "SELECT COUNT(*) FROM att_conf WHERE att_name = ?;";
    Stmt stmt(db, query);

    // Link the parameter with the name of the attribute
    stmt.linkPara(1, attr_name);

    // Execute SQL statement
    if (stmt.execute()) {
        int count = stmt.getColumnInt(0);
        return (count > 0);
    }
    return false;
}

//=============================================================================
//=============================================================================
void HdbppSqliteDbApi::insertAttHistoryEvent(unsigned char event)
{
    bool last_is_start = false;

    // Create the statement
    std::string query_count = "SELECT COUNT(*) FROM att_history_event;";
    Stmt stmt_count(db, query_count);

    // Execute SQL statement
    stmt_count.execute();
    int count = stmt_count.getColumnInt(0);
    if (count > 0) {
        // Create the statement
        std::string query_last = "SELECT event FROM att_history_event ORDER BY att_history_event_id DESC LIMIT 1;";
        Stmt stmt_last(db, query_last); 

        // Execute SQL statement
        stmt_last.execute();
        std::string last_event = stmt_last.getColumnString(0);
        if ((last_event == "START") && (event == 1)) {
            last_is_start = true;
        }
    }

    if (last_is_start) {
        // First insert the event CRASH
        // Create the statement
        std::string query_crash = "INSERT INTO att_history_event (event) VALUES (?);";
        Stmt stmt_crash(db, query_crash);
        // Link the parameter with the name of the attribute
        stmt_crash.linkPara(1, "CRASH");
        // Execute SQL statement
        stmt_crash.execute();

        // Then insert the event START
        // Create the statement
        std::string query_start = "INSERT INTO att_history_event (event) VALUES (?);";
        Stmt stmt_start(db, query_start);
        // Link the parameter with the name of the attribute
        stmt_start.linkPara(1, convert_to_string(event));
        // Execute SQL statement
        stmt_start.execute();
    } else {
        // Create the statement
        std::string query = "INSERT INTO att_history_event (event) VALUES (?);";
        Stmt stmt(db, query);

        // Link the parameter with the name of the attribute
        std::string insert_event = convert_to_string(event);
        stmt.linkPara(1, insert_event);

        // Execute SQL statement
        stmt.execute();
    }
}

//=============================================================================
//=============================================================================
std::string HdbppSqliteDbApi::convert_to_string(unsigned char event) 
{
    switch(event) {
        case DB_INSERT:
            return "INSERT";
        case DB_START:
            return "START";
        case DB_STOP:
            return "STOP";
        case DB_REMOVE:
            return "REMOVE";
        case DB_INSERT_PARAM:
            return "INSERT_PARAM";
        case DB_PAUSE:
            return "PAUSE";
        case DB_UPDATETTL:
            return "UPDATETTL";
        case DB_ADD:
            return "ADD";
        default:
            return "ERROR";
    }
}

} // namespace hdbpp