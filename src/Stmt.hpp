#ifndef _STMT
#define _STMT

#include <stdexcept>
#include <sqlite3.h>

class Stmt 
{
public:
    // Constructor. Compiling an SQL statement
    Stmt(sqlite3* db, const std::string& query);

    // Destructor. Destroy a prepared statement object
    ~Stmt();

    // Binding values to prepared statement
    void linkPara(int index, const std::string &value);
    void linkPara(int index, int value);
    void linkPara(int index, double value);
    void linkPara(int index, int64_t value);

    // Result values from a query
    std::string getColumnString(int index);
    int getColumnInt(int index);

    // Execute the SQL statement
    bool execute();

    // Variables to store the result of conversion for the read and write values
    std::string array_result_r;
    std::string array_result_w;

private:
    // SQLite database (handle the connection)
    sqlite3 &db_handle;
    // SQLite prepared statement
    sqlite3_stmt *stmt;


};
#endif // _STMT