#include <stdexcept>
#include <sqlite3.h>
#include <Stmt.hpp>

//=============================================================================
//=============================================================================
Stmt::Stmt(sqlite3* db, const std::string &query) 
    : db_handle(*db), stmt(nullptr)
{
    int rc = sqlite3_prepare_v2(&db_handle, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error preparing the statement : " + std::string(sqlite3_errmsg(db)));  
    }    
}

//=============================================================================
//=============================================================================
Stmt::~Stmt()
{
    if (stmt != nullptr) {
        sqlite3_finalize(stmt);
    }
}

//=============================================================================
//=============================================================================
void Stmt::linkPara(int index, const std::string &value)
{
    int rc = sqlite3_bind_text(stmt, index, value.c_str(), -1, SQLITE_STATIC);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error linking the parameter : " + std::string(sqlite3_errmsg(&db_handle)));  
    }
}

void Stmt::linkPara(int index, int value)
{
    int rc = sqlite3_bind_int(stmt, index, value);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error linking the parameter : " + std::string(sqlite3_errmsg(&db_handle)));  
    }
}

void Stmt::linkPara(int index, double value)
{
    int rc = sqlite3_bind_double(stmt, index, value);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error linking the parameter : " + std::string(sqlite3_errmsg(&db_handle)));  
    }
}

void Stmt::linkPara(int index, int64_t value)
{
    int rc = sqlite3_bind_int64(stmt, index, value);
    if (rc != SQLITE_OK) {
        throw std::runtime_error("Error linking the parameter : " + std::string(sqlite3_errmsg(&db_handle)));  
    }
}

//=============================================================================
//=============================================================================
std::string Stmt::getColumnString(int index) 
{
    const unsigned char *text = sqlite3_column_text(stmt, index);
    std::string value = reinterpret_cast < const char* > (text);
    return value;
}

int Stmt::getColumnInt(int index) 
{
    int value = sqlite3_column_int(stmt, index);
    return value;
}

//=============================================================================
//=============================================================================
bool Stmt::execute()
{
    int rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        return true;
    } else if (rc != SQLITE_DONE) {
        throw std::runtime_error("Error executing SQL statement : " + std::string(sqlite3_errmsg(&db_handle)));
    }
    return false;
}