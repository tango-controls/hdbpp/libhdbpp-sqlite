# Table of Contents

- [Table of Contents](#Table-of-Contents)
  - [About](#About)
  - [Building and Installation](#Building-and-Installation)

## About

The documentation is purely about getting the shared library running.
The overview is in the main project [README](../README.md).

## Building and Installation

* [Build](build.md) instructions.
* [Installation](install.md) guidelines.

