# Installation Instructions

The libhdbpp-sqlite.so binary is the only file that needs deploying to the target system.

## System Dependencies

The running system requires libsqlite3 installed to support the calls to sqlite. On Debian/Ubuntu this can be deployed as follows:

```bash
sudo apt-get install libsqlite3
```

## Installation

After the build has completed, simply run:

```
sudo make install
```

The shared library will be installed to /usr/local/lib on Debian/Ubuntu systems.
