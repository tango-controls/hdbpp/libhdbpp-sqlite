# libhdbpp-sqlite

[![TangoControls](https://img.shields.io/badge/-Tango--Controls-7ABB45.svg?style=flat&logo=%20data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACAAAAAkCAYAAADo6zjiAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAsFJREFUWIXtl01IFVEYht9zU%2FvTqOxShLowlOgHykWUGEjUKqiocB1FQURB0KJaRdGiaFM7gzZRLWpTq2olhNQyCtpYCP1gNyIoUTFNnxZzRs8dzvw4Q6564XLnfOf73vedc2a%2BmZEKALgHrC3CUUR8CxZFeEoFalsdM4uLmMgFoIlZLJp3A9ZE4S2oKehhlaR1BTnyg2ocnW%2FxsxEDhbYij4EPVncaeASMAavnS%2FwA8NMaqACNQCew3f4as3KZOYh2SuqTVJeQNiFpn6QGSRVjTH9W%2FiThvcCn6H6n4BvQDvQWFT%2BSIDIFDAKfE3KOAQeBfB0XGPeQvgE67P8ZoB44DvTHmFgJdOQRv%2BUjc%2BavA9siNTWemgfA3TwGquCZ3w8szFIL1ALngIZorndvgJOR0GlP2gtJkzH%2Bd0fGFxW07NqY%2FCrx5QRXcYjbCbmxF1dkBSbi8kpACah3Yi2Sys74cVyxMWY6bk5BTwgRe%2BYlSzLmxNpU3aBeJogk4XWWpJKUeiap3RJYCpQj4QWZDQCuyIAk19Auj%2BAFYGZZjTGjksaBESB8P9iaxUBIaJzjZcCQcwHdj%2BS2Al0xPOeBYYKHk4vfmQ3Y8YkIwRUb7wQGU7j2ePrA1URx93ayd8UpD8klyPbSQfCOMIO05MbI%2BDvwBbjsMdGTwlX21AAMZzEerkaI9zFkP4AeYCPBg6gNuEb6I%2FthFgN1KSQupqzoRELOSed4DGiJala1UmOMr2U%2Bl%2FTWEy9Japa%2Fy41IWi%2FJ3d4%2FkkaAw0Bz3AocArqApwTvet3O3GbgV8qqjAM7bf4N4KMztwTodcYVyelywKSCD5V3xphNXoezuTskNSl4bgxJ6jPGVJJqbN0aSV%2Bd0M0aO7FCs19Jo2lExphXaTkxdRVgQFK7DZVDZ8%2BcpdmQh3wuILh7ut3AEyt%2B51%2BL%2F0cUfwFOX0t0StltmQAAAABJRU5ErkJggg%3D%3D)](http://www.tango-controls.org) [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![](https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite/-/badges/release.svg)](https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite/-/releases)

- [libhdbpp-sqlite](#libhdbpp-sqlite)
  - [Bug Reports + Feature Requests](#bug-reports--feature-requests)
  - [Documentation](#documentation)
  - [Building](#building)
    - [Building Dependencies](#building-dependencies)
    - [Building Process](#building-process)
  - [Installing](#installing)
    - [System Dependencies](#system-dependencies)
    - [Installation](#installation)
  - [License](#license)

HDB++ backend library for sqlite. This library is used by events subscribers to archive events from a Tango Controls system.

The library requires [sqlite](https://www.sqlite.org/index.html). 

## Bug Reports + Feature Requests

Please file the bug reports and feature requests in the issue tracker

## Documentation

* See the [doc](doc/README.md) directory for documentation on various topics, including building and installing.
* General documentations on the Tango HDB system can be found [here](http://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/index.html#hdb-an-archiving-historian-service)

## Building

For a full reference on building please refer to the [documentation](doc/build.md). This is a small summary of the standard building steps.

### Building Dependencies

To build this library requires libsqlite3-dev installed. On Debian/Ubuntu this can be deployed as follows:

```bash
sudo apt-get install libsqlite3-dev
```

### Building Process

The build system uses pkg-config to find some dependencies, for example Tango. If Tango is not installed to a standard location, set PKG_CONFIG_PATH, i.e.

```bash
export PKG_CONFIG_PATH=/non/standard/tango/install/location
```

Then to build just the library:

```bash
mkdir -p build
cd build
cmake ..
make
```

The pkg-config path can also be set with the cmake argument CMAKE_PREFIX_PATH. This can be set on the command line at configuration time, i.e.:

```bash
...
cmake -DCMAKE_PREFIX_PATH=/non/standard/tango/install/location ..
...
```

## Installing

For a full reference on installation please refer to the [documentation](doc/install.md). This is a small summary of the standard installation steps.

### System Dependencies

The running system requires libsqlite3 installed to support the calls to sqlite. On Debian/Ubuntu this can be deployed as follows:

```bash
sudo apt-get install libsqlite3
```

### Installation

After the build has completed, simply run:

```
sudo make install
```

The shared library will be installed to /usr/local/lib on Debian/Ubuntu systems.

## License

The source code is released under the LGPL3 license and a copy of this license is provided with the code.

